<?php

declare(strict_types=1);

namespace App\Validator;

final class NumericValidator extends AbstractValidator
{
    private int $position;

    public function __construct(int $position)
    {
        $this->position = $position;
    }

    public function validate(array $payload)
    {
        if (!is_numeric($payload[$this->position])) {
            return false;
        }

        return parent::validate($payload);
    }
}
