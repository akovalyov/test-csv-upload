<?php

declare(strict_types=1);

namespace App\Validator;

abstract class AbstractValidator implements Validator
{
    private ?Validator $nextValidator = null;

    public function setNext(Validator $handler): Validator
    {
        $this->nextValidator = $handler;

        return $handler;
    }

    public function validate(array $payload)
    {
        if ($this->nextValidator) {
            return $this->nextValidator->validate($payload);
        }

        return null;
    }
}
