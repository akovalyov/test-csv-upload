<?php

declare(strict_types=1);

namespace App\Validator;

interface Validator
{
    public function setNext(Validator $handler): Validator;

    public function validate(array $payload);
}
