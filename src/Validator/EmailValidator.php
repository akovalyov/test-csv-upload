<?php

declare(strict_types=1);

namespace App\Validator;

final class EmailValidator extends AbstractValidator
{
    private int $position;

    public function __construct(int $position)
    {
        $this->position = $position;
    }

    public function validate(array $payload)
    {
        if (!filter_var($payload[$this->position], FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        return parent::validate($payload);
    }
}
