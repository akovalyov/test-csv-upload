<?php

declare(strict_types=1);

namespace App\Parser;

final class Line
{
    public int $id;
    public string $name;
    public string$email;
    public string $currency;
    public float $amount;

    private function __construct(int $id, string $name, string $email, string $currency, float $amount)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->currency = $currency;
        $this->amount = $amount;
    }

    public static function fromArray(array $line): Line
    {
        [$id, $name, $email, $currency, $amount] = $line;

        return new self((int) $id, (string) $name, (string) $email, (string) $currency, (float) $amount);
    }
}
