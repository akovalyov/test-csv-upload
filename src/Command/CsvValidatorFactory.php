<?php

declare(strict_types=1);

namespace App\Command;

use App\Validator\EmailValidator;
use App\Validator\IssetValidator;
use App\Validator\NumericValidator;
use App\Validator\StringValidator;
use App\Validator\Validator;

final class CsvValidatorFactory
{
    public static function createValidator(): Validator
    {
        $idExistsValidator = new IssetValidator(0);
        $nameExistsValidator = new IssetValidator(1);
        $emailExistsValidator = new IssetValidator(2);
        $currencyExistsValidator = new IssetValidator(3);
        $amountExistsValidator = new IssetValidator(4);

        $idValidator = new NumericValidator(0);
        $nameValidator = new StringValidator(1);
        $emailValidator = new EmailValidator(2);
        $currencyValidator = new StringValidator(3);
        $amountValidator = new NumericValidator(4);

        $idExistsValidator
            ->setNext($nameExistsValidator)
            ->setNext($emailExistsValidator)
            ->setNext($currencyExistsValidator)
            ->setNext($amountExistsValidator)
            ->setNext($idValidator)
            ->setNext($nameValidator)
            ->setNext($emailValidator)
            ->setNext($currencyValidator)
            ->setNext($amountValidator);

        return $idExistsValidator;
    }
}
