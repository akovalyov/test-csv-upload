<?php

declare(strict_types=1);

namespace App\Command;

use App\Database\SearcherInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class Search extends Command
{
    private SearcherInterface $searcher;

    public function __construct(SearcherInterface $searcher)
    {
        parent::__construct('search');
        $this->searcher = $searcher;
    }

    public function configure()
    {
        $this->setDescription('Searches via database');
        $this->addArgument('column', InputArgument::REQUIRED, 'Possible values: name, email');
        $this->addArgument('value', InputArgument::REQUIRED, 'Value to search for');
        $this->addOption('limit', 'l', InputArgument::OPTIONAL, 'Number of results', 30);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $column = $input->getArgument('column');
        if (!in_array($column, ['name', 'email'], true)) {
            $output->writeln('<error>Invalid field</error>');

            return 1;
        }

        $results = $this->searcher->searchBy($input->getArgument('column'), $input->getArgument('value'), (int) $input->getOption('limit'));

        if (count($results) > 0) {
            $table = new Table($output);
            $table
                ->setHeaders(['ID', 'Name', 'Email', 'Currency', 'Amount'])
                ->setRows($results);

            $table->render();
        } else {
            $output->writeln('<info>No results found</info>');
        }

        return 0;
    }
}
