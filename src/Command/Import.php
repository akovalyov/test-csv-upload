<?php

declare(strict_types=1);

namespace App\Command;

use App\Database\Importer;
use App\Parser\Line;
use App\Validator\Validator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Import extends Command
{
    private Importer $writer;

    private const BATCH_SIZE = 100;

    /**
     * @var Line[]
     */
    private array $buffer = [];
    private Validator $validator;

    public function __construct(Importer $writer, Validator $validator)
    {
        parent::__construct('import');
        $this->writer = $writer;
        $this->validator = $validator;
    }

    public function configure()
    {
        $this->setDescription('Imports csv file');
        $this->addArgument('path', InputArgument::REQUIRED);
        $this->addOption('batch-size', 'b', InputOption::VALUE_OPTIONAL, 'Batch size, default 100', self::BATCH_SIZE);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('path');

        $batchSize = $input->getOption('batch-size');

        if (!is_file($path)) {
            $output->writeln(sprintf('<error>%s does not exist or is not readable.</error>', $path));

            return 1;
        }

        $file = new \SplFileObject($path);

        $i = 0;
        $numberOfBatches = 0;
        while (!$file->eof()) {
            $line = $file->fgetcsv();

            if (false === $line) {
                continue;
            }

            ++$i;

            if (false === $this->validator->validate($line)) {
                continue;
            }

            $this->buffer[] = Line::fromArray($line);

            if (0 === $i % $batchSize) {
                $this->writer->import(...$this->buffer);
                $this->buffer = [];
                ++$numberOfBatches;
                $output->writeln(sprintf('<info>Writing batch %s</info>', $numberOfBatches));
            }
        }
        $this->writer->import(...$this->buffer);

        return 0;
    }
}
