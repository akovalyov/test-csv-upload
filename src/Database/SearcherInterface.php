<?php

declare(strict_types=1);

namespace App\Database;

interface SearcherInterface
{
    public function searchBy(string $name, string $value, int $limit): array;
}
