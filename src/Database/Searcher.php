<?php

declare(strict_types=1);

namespace App\Database;

use PDO;

class Searcher implements SearcherInterface
{
    private PDO $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function searchBy(string $name, string $value, int $limit): array
    {
        if (!in_array($name, ['name', 'email'], true)) {
            throw new \InvalidArgumentException(sprintf('Search by name/email is implemented, %s given.', $name));
        }

        if ($limit < 1) {
            throw new \InvalidArgumentException(sprintf('Limit cannot be negative.'));
        }
        //we can bind only parameters, not column names, thus have to combine approaches
        $stmt = $this->pdo->prepare(
            'SELECT * FROM users WHERE '.$name.' LIKE CONCAT(\'%\', :value, \'%\') limit :limit'
        );

        $stmt->bindValue(':value', $value);
        $stmt->bindValue(':limit', $limit, PDO::PARAM_INT);

        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
