<?php

declare(strict_types=1);

namespace App\Database;

use Psr\Cache\CacheItemPoolInterface;
use Symfony\Contracts\Cache\ItemInterface;

final class CachingSearcher implements SearcherInterface
{
    private SearcherInterface $searcher;

    private CacheItemPoolInterface $cacher;

    public function __construct(Searcher $searcher, CacheItemPoolInterface $cacher)
    {
        $this->searcher = $searcher;
        $this->cacher = $cacher;
    }

    public function searchBy(string $name, string $value, int $limit): array
    {
        $cacheKey = sha1($name.$value.$limit);

        return $this->cacher->get($cacheKey, function (ItemInterface $item) use ($name, $value, $limit) {
            $item->expiresAfter(3600);

            return $this->searcher->searchBy($name, $value, $limit);
        });
    }
}
