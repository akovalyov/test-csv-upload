<?php

declare(strict_types=1);

namespace App\Database;

use App\Parser\Line;
use PDO;

class Importer
{
    private PDO $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function import(Line ...$data)
    {
        $columns = ['id', 'name', 'email', 'currency', 'amount'];
        $columnsString = '('.implode(', ', $columns).')';
        $singlePlaceholder = '('.implode(', ', array_fill(0, count($columns), '?')).')';

        $placeholders = implode(', ', array_fill(0, count($data), $singlePlaceholder));

        $sql = sprintf(
            'INSERT INTO %s %s VALUES %s;',
            'users',
            $columnsString,
            $placeholders
        );

        $datas = array_merge(...array_map(function (Line $line) {
            return [
                $line->id,
                $line->name,
                $line->email,
                $line->currency,
                $line->amount,
            ];
        }, $data));

        $stmt = $this->pdo->prepare($sql);

        $this->pdo->beginTransaction();
        $stmt->execute($datas);
        $this->pdo->commit();
    }
}
