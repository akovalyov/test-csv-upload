CREATE TABLE `users` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(1024) NOT NULL,
	`email` VARCHAR(100) NOT NULL,
	`currency` TEXT(3) NOT NULL,
	`amount` DOUBLE NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `name_idx` (`name`),
	INDEX `email_idx` (`email`)
);