<?php

declare(strict_types=1);

require __DIR__.'/vendor/autoload.php';

use App\Command\Import;
use App\Command\Search;
use Symfony\Component\Console\Application;

$application = new Application();

$container = new DI\Container();
$builder = new DI\ContainerBuilder();

$builder->addDefinitions(__DIR__.'/di-config.php');

$container = $builder->build();

$application->add($container->get(Import::class));
$application->add($container->get(Search::class));

$application->run();
