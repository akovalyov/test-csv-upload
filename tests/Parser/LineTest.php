<?php

declare(strict_types=1);

namespace test\Command\Parser;

use App\Parser\Line;
use PHPUnit\Framework\TestCase;

/** @covers \App\Parser\Line */
class LineTest extends TestCase
{
    /** @test */
    public function it_should_be_constructed_via_named_constructor()
    {
        $line = Line::fromArray([1, 'Andrew', 'andrew@kovalyov.me', 'USD', 3500]);
        $this->assertEquals(1, $line->id);
        $this->assertEquals('Andrew', $line->name);
    }
}
