<?php

declare(strict_types=1);

namespace test\Command\Import;

use App\Command\Search;
use App\Database\SearcherInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

/**
 * @covers \App\Command\Search
 */
class SearchTest extends TestCase
{
    use ProphecyTrait;

    /** @test
     * @dataProvider examples
     */
    public function it_should_search($column, $value)
    {
        $searcher = $this->prophesize(SearcherInterface::class);
        $searcher->searchBy($column, $value, 30)->willReturn([['a', 'b', 'c', 'd', 'e']]);
        $command = new Search($searcher->reveal());

        $input = new ArrayInput(['column' => $column, 'value' => $value]);
        $output = new BufferedOutput();

        $this->assertEquals(0, $command->run($input, $output));
    }

    /** @test */
    public function it_should_fail_if_file_does_not_exist()
    {
        $searcher = $this->prophesize(SearcherInterface::class);

        $command = new Search($searcher->reveal());

        $input = new ArrayInput(['column' => 'id', 'value' => 1]);
        $output = new BufferedOutput();
        $this->assertEquals(1, $command->run($input, $output));
        $this->assertStringContainsString('Invalid field', $output->fetch());
    }

    public function examples()
    {
        yield ['name', 'andrew'];
        yield ['email', 'andrew@kovalyov.me'];
    }
}
