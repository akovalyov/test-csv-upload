<?php

declare(strict_types=1);

namespace test\Command\Import;

use App\Command\Import;
use App\Database\Importer;
use App\Validator\Validator;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

/**
 * @covers \App\Command\Import
 */
final class ImportTest extends TestCase
{
    use ProphecyTrait;

    /** @test */
    public function it_should_parse_file()
    {
        $writer = $this->prophesize(Importer::class);

        $validator = $this->prophesize(Validator::class);
        $validator->validate(Argument::any())->willReturn(true);

        $command = new Import($writer->reveal(), $validator->reveal());

        $input = new ArrayInput(['path' => __DIR__.'/fixtures/valid.csv']);
        $output = new BufferedOutput();
        $this->assertEquals(0, $command->run($input, $output));
    }

    /** @test */
    public function it_should_fail_if_file_does_not_exist()
    {
        $writer = $this->prophesize(Importer::class);
        $validator = $this->prophesize(Validator::class);

        $command = new Import($writer->reveal(), $validator->reveal());
        $input = new ArrayInput(['path' => __DIR__.'/fixtures/404.csv']);
        $output = new BufferedOutput();
        $this->assertEquals(1, $command->run($input, $output));
        $this->assertStringContainsString('fixtures/404.csv does not exist or is not readable.', $output->fetch());
    }
}
