<?php

declare(strict_types=1);

namespace tests\App\Validator;

use App\Validator\NumericValidator;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Validator\NumericValidator
 * @covers \App\Validator\AbstractValidator
 */
class NumericValidatorTest extends TestCase
{
    /** @test */
    public function it_should_validate_numbers()
    {
        $validator = new NumericValidator(0);

        $this->assertEquals(null, $validator->validate(['123']));
    }

    /** @test */
    public function it_should_fail_on_non_numerics()
    {
        $validator = new NumericValidator(0);

        $this->assertEquals(false, $validator->validate(['andrew']));
    }
}
