<?php

declare(strict_types=1);

namespace tests\App\Validator;

use App\Validator\IssetValidator;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Validator\IssetValidator
 * @covers \App\Validator\AbstractValidator
 */
class IssetValidatorTest extends TestCase
{
    /** @test */
    public function it_should_validate_existence()
    {
        $validator = new IssetValidator(0);

        $this->assertEquals(null, $validator->validate([1]));
    }

    /** @test */
    public function it_should_fail_on_non_existent()
    {
        $validator = new IssetValidator(5);

        $this->assertEquals(false, $validator->validate([1]));
    }
}
