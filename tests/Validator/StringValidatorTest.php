<?php

declare(strict_types=1);

namespace tests\App\Validator;

use App\Validator\StringValidator;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Validator\StringValidator
 * @covers \App\Validator\AbstractValidator
 */
class StringValidatorTest extends TestCase
{
    /** @test */
    public function it_should_validate_strings()
    {
        $validator = new StringValidator(0);

        $this->assertEquals(null, $validator->validate(['asd']));
    }

    /** @test */
    public function it_should_fail_on_empty_values()
    {
        $validator = new StringValidator(0);

        $this->assertEquals(false, $validator->validate([null]));
    }
}
