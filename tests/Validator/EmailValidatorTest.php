<?php

declare(strict_types=1);

namespace tests\App\Validator;

use App\Validator\EmailValidator;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Validator\EmailValidator
 * @covers \App\Validator\AbstractValidator
 */
class EmailValidatorTest extends TestCase
{
    /**
     * @var EmailValidator
     */
    private ?EmailValidator $validator = null;

    public function setUp(): void
    {
        $this->validator = new EmailValidator(0);
    }

    /** @test */
    public function it_should_validate_emails()
    {
        $this->assertEquals(null, $this->validator->validate(['andrew@kovalyov.me']));
    }

    /** @test */
    public function it_should_fail_on_invalid_emails()
    {
        $this->assertEquals(false, $this->validator->validate(['andrew']));
    }
}
