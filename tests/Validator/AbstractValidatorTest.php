<?php

declare(strict_types=1);

namespace tests\App\Validator;

use App\Validator\AbstractValidator;
use App\Validator\Validator;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

/** @covers \App\Validator\AbstractValidator */
class AbstractValidatorTest extends TestCase
{
    use ProphecyTrait;
    /**
     * @var AbstractValidator
     */
    private $validator;

    public function setUp(): void
    {
        $this->validator = new class() extends AbstractValidator {
        };
    }

    /** @test */
    public function it_should_delegate_to_next_handler()
    {
        $handler = $this->prophesize(Validator::class);
        $handler->validate(['asd'])->willReturn(null);

        $this->validator->setNext($handler->reveal());

        $this->assertEquals(null, $this->validator->validate(['asd']));
    }

    /** @test */
    public function it_should_return_null_if_no_handlers()
    {
        $this->assertEquals(null, $this->validator->validate(['asd']));
    }
}
