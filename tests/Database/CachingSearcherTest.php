<?php

declare(strict_types=1);

namespace test\Command\Database;

use App\Database\CachingSearcher;
use App\Database\Searcher;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Cache\Adapter\AbstractAdapter;

/** @covers \App\Database\CachingSearcher */
class CachingSearcherTest extends TestCase
{
    use ProphecyTrait;

    /** @test */
    public function it_should_search()
    {
        $sample = ['foo' => 'bar'];
        $innerSearcher = $this->prophesize(Searcher::class);
        $cache = $this->prophesize(AbstractAdapter::class);
        $cache->get(Argument::type('string'), Argument::type('callable'))->willReturn($sample);
        $searcher = new CachingSearcher($innerSearcher->reveal(), $cache->reveal());

        $this->assertEquals($sample, $searcher->searchBy('name', 'Kovalyov', 1));
    }
}
