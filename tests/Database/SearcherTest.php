<?php

declare(strict_types=1);

namespace test\Command\Database;

use App\Database\Searcher;
use InvalidArgumentException;
use PDO;
use PDOStatement;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/** @covers \App\Database\Searcher */
class SearcherTest extends TestCase
{
    use ProphecyTrait;

    /** @test */
    public function it_should_throw_exception_on_invalid_column()
    {
        $pdo = $this->prophesize(PDO::class);
        $searcher = new Searcher($pdo->reveal());
        $this->expectException(InvalidArgumentException::class);
        $searcher->searchBy('asd', 'Kovalyov', 1);
    }

    /** @test */
    public function it_should_throw_exception_on_invalid_limit()
    {
        $pdo = $this->prophesize(PDO::class);
        $searcher = new Searcher($pdo->reveal());
        $this->expectException(InvalidArgumentException::class);
        $searcher->searchBy('name', 'Kovalyov', -1);
    }

    /** @test */
    public function it_should_search()
    {
        $sample = ['foo' => 'bar'];
        $pdo = $this->prophesize(PDO::class);
        $statement = $this->prophesize(PDOStatement::class);
        $pdo->prepare(Argument::type('string'))->willReturn($statement);
        $statement->bindValue(':value', 'Kovalyov')->shouldBeCalled();
        $statement->bindValue(':limit', 1, 1)->shouldBeCalled();
        $statement->execute()->shouldBeCalled();
        $statement->fetchAll(PDO::FETCH_ASSOC)->willReturn($sample);

        $searcher = new Searcher($pdo->reveal());
        $this->assertEquals($sample, $searcher->searchBy('name', 'Kovalyov', 1));
    }
}
