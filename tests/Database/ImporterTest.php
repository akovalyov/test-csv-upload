<?php

declare(strict_types=1);

namespace test\Command\Database;

use App\Database\Importer;
use App\Parser\Line;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/** @covers \App\Database\Importer
/** @covers \App\Parser\Line
 */
class ImporterTest extends TestCase
{
    use ProphecyTrait;

    /** @test */
    public function it_should_import_data()
    {
        $line = Line::fromArray([1, 'Andrew', 'andrew@kovalyov.me', 'USD', 3500]);
        $pdo = $this->prophesize(\PDO::class);
        $statement = $this->prophesize(\PDOStatement::class);
        $pdo->prepare(Argument::type('string'))->willReturn($statement);
        $statement->execute(Argument::type('array'))->shouldBeCalled();
        $statement->rowCount()->willReturn(1);
        $pdo->beginTransaction()->shouldBeCalled();
        $pdo->commit()->shouldBeCalled();
        $importer = new Importer($pdo->reveal());

        $this->assertNull($importer->import($line));
    }
}
