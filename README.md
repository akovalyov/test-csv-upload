Тестовое задание на импорт CSV.

Requirements: https://docs.google.com/document/d/1uRwJncuToLWGzSKXWvlU-otOJPVkxH95-uoAGB9btt0/edit

# TL;DR

Prerequisites: composer installed globally, docker, docker-compose

## Поднять кластер

```bash

$ composer up #обертка над docker-compose build && docker-compose up -d

```

(тут в течение минуты возможны проблемы с тем, что mysql долго поднимается. Писать healthcheck показалось оверкиллом.)

## Прогнать тесты

```bash

$ composer test

```


## Запуск команды на импорт (подразумевается, что файл будет лежать в корне проекта для упрощения логики с volumes/copy/etc)

```bash

$ composer up
$ docker-compose run php php cli.php import $CSV_PATH #опционально можно указать --batch-size, по умолчанию равен 1000

```

При тестировании импортировалось 200к записей за 40-50 секунд с батчами по 10к записей.

`$ time docker-compose run php php cli.php import file.csv --batch-size=10000
docker-compose run php php cli.php import file.csv --batch-size=10000  49,30s user 0,30s system 93% cpu 53,300 total`

## Запуск команды на поиск 

```bash

$ composer up
$ docker-compose run php php cli.php search $COLUMN_NAME $SEARCH_VALUE #доступные поля для $COLUMN_NAME -- name, email. Опция --limit по умолчанию 30

```

## Потушить кластер
```bash
$ composer down
```

# Использованные паттерны

#### Chain of responsibility -- валидатор строк CSV
#### Decorator -- кеширование записей в редис при поиске
#### Factory -- создание CoR
#### Именованный конструктор -- DTO для строки

а также Dependency Injection, спасибо PHP-DI.

# Генерация тестовых данных

В корне проекта лежит скрипт, который генерирует 200к и экспортирует в CSV. 

```bash
$ docker-compose run php php generate_csv.php
```