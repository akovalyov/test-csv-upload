<?php

declare(strict_types=1);
require_once __DIR__.'/vendor/autoload.php';

$buffer = [];

$faker = Faker\Factory::create();
$data = [];
$id = 1;
$fp = fopen('file.csv', 'w');

for ($i = 0; $i < 200000; ++$i) {
    fputcsv($fp, [
        $id++,
        $faker->name,
        $faker->email,
        $faker->currencyCode,
        $faker->randomFloat(2, 1, 1000),
    ]);
}
fclose($fp);
