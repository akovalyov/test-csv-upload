<?php

declare(strict_types=1);

use App\Command\CsvValidatorFactory;
use App\Command\Import;
use App\Command\Search;
use App\Database\CachingSearcher;
use App\Database\Importer;
use App\Database\Searcher;
use App\Database\SearcherInterface;
use App\Validator\Validator;
use function DI\create;
use function DI\env;
use function DI\factory;
use function DI\get;
use Predis\Client as Redis;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\Cache\Adapter\RedisAdapter;

return [
    'database_user' => env('DATABASE_USER', 'root'),
    'database_password' => env('DATABASE_PASSWORD', 'root'),
    'database_host' => env('DATABASE_HOST', '127.0.0.1'),
    'database_name' => env('DATABASE_NAME', 'test'),
    'redis_host' => env('REDIS_HOST', '127.0.0.1'),
    PDO::class => factory(function (DI\Container $c) {
        return new PDO(
            sprintf(
                'mysql:host=%s;dbname=%s',
                $c->get('database_host'),
                $c->get('database_name'),
            ),
            $c->get('database_user'),
            $c->get('database_password')
        );
    }),
    Validator::class => factory([CsvValidatorFactory::class, 'createValidator']),
//importer
    Importer::class => create()
        ->constructor(get(PDO::class)),
    Import::class => create()
        ->constructor(get(Importer::class), get(Validator::class)),

//alias interface => caching searcher
    SearcherInterface::class => get(CachingSearcher::class),
//alias cache => redis adapter
    CacheItemPoolInterface::class => get(RedisAdapter::class),
    Redis::class => factory(function (DI\Container $c) {
        return new Redis([
            'host' => $c->get('redis_host'),
        ]);
    }),
    Searcher::class => create()
        ->constructor(get(PDO::class)),
    Search::class => create()
        ->constructor(get(SearcherInterface::class)),
    RedisAdapter::class => create()->constructor(get(Redis::class)),
];
